package main

import (
	"fmt"
	//"github.com/sensepost/maltegolocal/maltegolocal"
	"os"

	"github.com/glennzw/maltegogo"
	"gitlab.com/Hispagatos/nmap.git"
)

func main() {
	lt := maltegogo.ParseLocalArguments(os.Args)
	Host := lt.Value
	PortsToScan := "21,22,23,25,110,443,444,445,80,8080-8085"

	Ports, _ := nmap.ScanOpenTcpPorts(Host, PortsToScan)
	TRX := maltegogo.MaltegoTransform{}
	for i := 0; i < len(Ports); i++ {
		port := fmt.Sprint(Ports[i].HostPort)
		service := fmt.Sprint(Ports[i].ServiceName)
		banner := fmt.Sprint(Ports[i].ServiceBanner)

		//ports
		NewEnt := TRX.AddEntity("maltego.Port", "Scanning... "+Host)
		NewEnt.SetType("maltego.Port")
		NewEnt.SetValue(port)
		NewEnt.AddDisplayInformation(Host+" <b>Banner:</b> "+banner+" <b>Service:</b> "+service+"!", "Service and Banner")
		NewEnt.AddProperty("properties.port", "Port", "stict", "TCP")
		NewEnt.AddProperty(Host, "Host", "stict", Host)
		NewEnt.SetLinkColor("#FF0000")
		NewEnt.SetWeight(200)
		//NewEnt.SetNote("Service: " + service)

		//Services
		NewServ := TRX.AddEntity("maltego.Service", "Scanning... "+Host)
		NewServ.SetType("maltego.Service")
		NewServ.SetValue(service)
		NewServ.AddProperty("banner.text", "Service Banner", "stict", banner)
		NewServ.AddDisplayInformation(Host+" <b>Banner:</b> "+banner+" <b>Service:</b> "+service+"!", "Service and Banner")
		NewServ.AddProperty(service, "service", "stict", service)
		NewServ.AddProperty(banner, "Service Banner", "stict", banner)
		NewServ.SetLinkColor("#FF0000")
		NewServ.SetWeight(200)

		TRX.AddUIMessage("completed!", "Inform")
	}
	fmt.Println(TRX.ReturnOutput())
}
