# maltego-nmap
A maltego transform to interact with the popular port scanner "nmap"
by ReK2 of the Hispagtos hacker collective.

works but still adding features as needed.
need to add a custom port options to scan.

![Alt text](/nmap-maltego-transform-alpha.png?raw=true "Maltego with nmap transform in action")
![Alt text](/nmap-maltego-transform-alpha2.png?raw=true "Maltego with nmap transform in action")

---
_what you need?_

Obviosly:
* Nmap
* Maltego
* Go
---

### Instalation

```
git clone https://gitlab.com/rek2/maltego-nmap.git
cd maltego-nmap
go mod init https://gitlab.com/rek2/maltego-nmap.git
go build -o maltego-nmap
cp maltego-nmap ~/bin/maltego-nmap #or anyplace you want this binary to be
```

- Remember where you put the maltego-nmap resulting binary you will need that path/localtion for adding a local transform to maltego
- You adding to maltego a binary not a script so will be someththing line "/home/myuser/bin/maltego-nmap" instead of a scripting language interpreter.

[Upstream instructions in how to install a GO transform, but read my comment above!!!i](https://github.com/NoobieDog/maltegolocal/blob/master/README.md)

